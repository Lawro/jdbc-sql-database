package uk.ac.bris.cs.databases.cwk3;

import java.sql.*;
import java.util.*;
import uk.ac.bris.cs.databases.api.APIProvider;
import uk.ac.bris.cs.databases.api.AdvancedForumSummaryView;
import uk.ac.bris.cs.databases.api.AdvancedForumView;
import uk.ac.bris.cs.databases.api.ForumSummaryView;
import uk.ac.bris.cs.databases.api.ForumView;
import uk.ac.bris.cs.databases.api.AdvancedPersonView;
import uk.ac.bris.cs.databases.api.PostView;
import uk.ac.bris.cs.databases.api.Result;
import uk.ac.bris.cs.databases.api.PersonView;
import uk.ac.bris.cs.databases.api.SimpleForumSummaryView;
import uk.ac.bris.cs.databases.api.SimpleTopicSummaryView;
import uk.ac.bris.cs.databases.api.SimpleTopicView;
import uk.ac.bris.cs.databases.api.SimplePostView;
import uk.ac.bris.cs.databases.api.TopicView;
import uk.ac.bris.cs.databases.api.TopicSummaryView;

/**
 *
 * @author csxdb
 */
public class API implements APIProvider {

    private final Connection c;

    public API(Connection c) {
        this.c = c;
    }

    /* A.1 */

    @Override
    public Result<Map<String, String>> getUsers() {
        Map<String, String> map = new HashMap<>();
        String STMT = "SELECT username, name FROM Person";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                map.put(rs.getString("username"), rs.getString("name"));
            }
            return Result.success(map);
        } catch (SQLException e) {
            return Result.fatal(e.getMessage());
        }
    }

    @Override
    public Result<PersonView> getPersonView(String username) {
        if (username == null || username.isEmpty()) {
            return Result.failure("getPersonView: username cannot be empty");
        }
        String STMT = "SELECT username, name, stuId FROM Person WHERE username = ?";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            p.setString(1, username);
            ResultSet rs = p.executeQuery();
            if (rs.next()) {
                PersonView pview = new PersonView(rs.getString("name"),
                    rs.getString("username"), rs.getString("stuId"));
                return Result.success(pview);
            }
            else {
                return Result.failure("getPersonView: User doesn't exist");
            }
        } catch (SQLException e) {
            return Result.fatal(e.getMessage());
        }
    }

    @Override
    public Result addNewPerson(String name, String username, String studentId) {
        if (name == null || name.isEmpty()) {
            return Result.failure("addNewPerson: Name field cannot be empty");
        }
        if (username == null || username.isEmpty()) {
            return Result.failure("addNewPerson: Username field cannot be empty");
        }
        String STMT = "INSERT INTO Person (name, username, stuId)"+
                "VALUES (?,?,?)";
        try (PreparedStatement p = c.prepareStatement(STMT)){
            studentId = studentId==null ? "" : studentId;
            p.setString(1, name);
            p.setString(2, username);
            p.setString(3, studentId);
            p.executeUpdate();
            c.commit();
            return Result.success();
        } catch (SQLException e) {
            if (e instanceof SQLIntegrityConstraintViolationException)
                return Result.failure("addNewPerson: Username already exists");
            try {
                c.rollback();
            } catch (SQLException f) {
                return Result.fatal(f.getMessage());
            }
            return Result.fatal(e.getMessage());
        }
    }

    /* A.2 */

    @Override
    public Result<List<SimpleForumSummaryView>> getSimpleForums() {
        List<SimpleForumSummaryView> ForumList = new ArrayList<>();
        final String STMT = "SELECT * FROM Forum";
        try (PreparedStatement p = c.prepareStatement(STMT)){
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                SimpleForumSummaryView forum = new SimpleForumSummaryView (
                    rs.getLong("id"), rs.getString("title"));
                ForumList.add(forum);
            }
            return Result.success(ForumList);
        } catch (SQLException e) {
            return Result.fatal(e.getMessage());
        }
    }

    @Override
    public Result createForum(String title) {
        if (title == null || title.isEmpty()) {
            return Result.failure("Cannot have null or empty title.");
        }
        String STMT = "INSERT INTO Forum (title) VALUES (?)";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            p.setString(1, title);
            int checkCreate = p.executeUpdate();
            if (checkCreate < 1) {
                return Result.failure("createForum: Insertion of new forum failed");
            }
            else {
                c.commit();
                return Result.success();
            }
        } catch (SQLException e) {
            try {
                    c.rollback();
                } catch (SQLException f) {
                    return Result.fatal("Unable to rollback changes.");
                }
            if (e instanceof SQLIntegrityConstraintViolationException)
                return Result.failure("createForum: Forum with this title already exists");
            else
                return Result.fatal(e.getMessage());
        }
    }

    /* A.3 */
    //alphabetically order?
    @Override
    public Result<List<ForumSummaryView>> getForums() {
        List<ForumSummaryView> forumList = new ArrayList<>();
       // final String forumSTMT = "SELECT Forum.id AS fid, Forum.title AS ftitle, Topic.id AS tid, Topic.title AS ttitle FROM Forum LEFT JOIN Topic ON Forum.id = (SELECT forumId FROM Topic WHERE forumId = Forum.id ORDER BY created DESC LIMIT 1) GROUP BY Forum.id";
  //      final String forumSTMT = "SELECT Forum.id AS fid, Forum.title AS ftitle, tid, ttitle FROM Forum LEFT JOIN (SELECT Topic.id AS tid, Topic.title AS ttitle FROM Topic ORDER BY created) AS tp ON Forum.id = tid GROUP BY Forum.id";
   //     final String forumSTMT = "SELECT Forum.id AS fid, Forum.title AS ftitle, tid, ttitle FROM Forum LEFT JOIN (SELECT Topic.id AS tid, Topic.title AS ttitle FROM Topic JOIN Post ON Topic.id = Post.topicId ORDER BY createdAT) AS tp ON Forum.id = tid GROUP BY Forum.id";
        final String forumSTMT = "SELECT Forum.id AS fid, Forum.title AS ftitle, tid, ttitle FROM Forum LEFT JOIN (SELECT createdAt, Topic.forumId AS forumId, Topic.id AS tid, Topic.title AS ttitle FROM Topic JOIN Post ON Topic.id = Post.topicId ORDER BY createdAt DESC) AS tp ON Forum.id = forumId GROUP BY Forum.id, Forum.title, ttitle, tid";
        try (PreparedStatement p = c.prepareStatement(forumSTMT)) {
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                if (rs.getString("ttitle") == null) {
                    ForumSummaryView FSV = new ForumSummaryView(rs.getLong("fid"),
                        rs.getString("ftitle"), null);
                    forumList.add(FSV);
                }
                else {
                    SimpleTopicSummaryView STSV = new SimpleTopicSummaryView(rs.getLong("tid"),
                        rs.getLong("fid"), rs.getString("ttitle"));
                    ForumSummaryView FSV = new ForumSummaryView(rs.getLong("fid"),
                        rs.getString("ftitle"), STSV);
                    forumList.add(FSV);
                }
            }
            return Result.success(forumList);
        } catch (SQLException e) {
            return Result.fatal(e.getMessage());
        }
    }

        @Override
        public Result<ForumView> getForum(long id) {
            List<SimpleTopicSummaryView> STSVlist = new ArrayList<>();
            String ftitle = null;
            String STMT = "SELECT * FROM Forum WHERE id = ?";
            String STMT2 = "SELECT id, title FROM Topic WHERE forumId = ?";
            try (PreparedStatement p = c.prepareStatement(STMT)) {
                p.setLong(1, id);
                ResultSet rs = p.executeQuery();
                if (rs.next()) {
                    ftitle = rs.getString("title");
                }
            }
            catch (SQLException e) {
                return Result.fatal(e.getMessage());
            }
            try (PreparedStatement p2 = c.prepareStatement(STMT2)) {
                p2.setLong(1, id);
                ResultSet rs2 = p2.executeQuery();
                while (rs2.next()) {
                    SimpleTopicSummaryView STSV = new SimpleTopicSummaryView(
                        rs2.getLong("id"), id, rs2.getString("title"));
                    STSVlist.add(STSV);
                }
            }
            catch (SQLException f) {
                return Result.fatal(f.getMessage());
            }
            ForumView FV = new ForumView(id, ftitle, STSVlist);
            return Result.success(FV);
        }
      //  try (PreparedStatement p = c.prepareStatement(STMT1)) {
        //    p.setLong(1, id);
        //    ResultSet rs = p.executeQuery();
          //  if (rs.next()) {

                // try (PreparedStatement p2 = c.prepareStatement(STMT2)) {
                //     p2.setLong(1, id);
                //     p2.setLong(2, id);
                //     ResultSet rs2 = p2.executeQuery();
                //     List<SimpleTopicSummaryView> STSVlist = new ArrayList<>();
                //     if (rs2.next()) {
                //         ftitle = rs2.getString("ftitle");
                //     //SimpleTopicSummaryView STSV = getTopic(p2);
                //         SimpleTopicSummaryView STSV = new SimpleTopicSummaryView(
                //         rs2.getLong("tid"), id, rs2.getString("ttitle"));
                //         STSVlist.add(STSV);
                //       }
                //       ForumView FV = new ForumView(id, ftitle, STSVlist);
                //       return Result.success(FV);

                //   //  return Result.success();

                // } catch (SQLException e) {
                // return Result.fatal(e.getMessage());
                // }


        //    }
      //      else return Result.failure("No forum with this id exists.");
      //  } catch (SQLException e) {
      //  return Result.fatal(e.getMessage());
    //    }
//    }
    /*    public Result<ForumView> getForum(long id) {
        List<SimpleTopicSummaryView> STSVlist = new LinkedList<>();
    //    String STMT1 = "SELECT * FROM Forum WHERE id = ?";
      //  try (PreparedStatement p = c.prepareStatement(STMT1)) {
        //    p.setLong(1, id);
        //    ResultSet rs = p.executeQuery();
          //  if (rs.next()) {
                String STMT2 = "SELECT Topic.id AS tid, forumId, Forum.title AS ftitle, Topic.title AS ttitle FROM Topic JOIN Forum ON Topic.forumId = Forum.id WHERE Forum.id = ?";
                try (PreparedStatement p2 = c.prepareStatement(STMT2)) {
                    p2.setLong(1, id);
                    ResultSet rs2 = p2.executeQuery();
                    if (rs2.next()) {
                    //SimpleTopicSummaryView STSV = getTopic(p2);
                        SimpleTopicSummaryView STSV = new SimpleTopicSummaryView(
                        rs2.getLong("tid"), id, rs2.getString("ttitle"));
                        STSVlist.add(STSV);
                    }
                    ForumView FV = new ForumView(id, rs2.getString("ftitle"), STSVlist);
                    return Result.success(FV);
                } catch (SQLException e) {
                return Result.fatal(e.getMessage());
                }
        //    }
      //      else return Result.failure("No forum with this id exists.");
      //  } catch (SQLException e) {
      //  return Result.fatal(e.getMessage());
    //    }
    }*/

    @Override
    public Result<SimpleTopicView> getSimpleTopic(long topicId) {
        List<SimplePostView> STSVlist = new ArrayList<>();
        String title = null;
        String STMT = "SELECT Topic.title, postId, Post.user, Post.text AS postText, createdAt FROM Topic JOIN Post ON Topic.id = Post.topicId JOIN Person ON Person.id = Post.user WHERE Topic.id = ? ORDER BY createdAt";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            p.setLong(1, topicId);
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                String name = getName(rs.getLong("user"));
                title = rs.getString("title");
                SimplePostView SPV = new SimplePostView(rs.getInt("postId"),
                    name, rs.getString("postText"), rs.getString("createdAt"));
                STSVlist.add(SPV);
            }
        } catch (SQLException e) {
            return Result.fatal("Cannot get SimplePostView");
        }
        SimpleTopicView STV = new SimpleTopicView(topicId, title, STSVlist);
        return Result.success(STV);
    }

    @Override
    public Result<PostView> getLatestPost(long topicId) {
        String STMT = "SELECT forumId FROM Topic WHERE id = ?";
        String name = null;
        try(PreparedStatement p = c.prepareStatement(STMT)) {
            p.setLong(1, topicId);
            ResultSet rs = p.executeQuery();
            if(rs.next()) {
                String STMT2 = "SELECT topicId, postId, user, username, text, createdAt FROM Post JOIN Person ON Post.user = Person.id WHERE topicId = ? ORDER BY createdAt DESC LIMIT 1";
                try(PreparedStatement p2 = c.prepareStatement(STMT2)) {
                    p2.setLong(1, topicId);
                    ResultSet rs2 = p2.executeQuery();
                    if(rs2.next()) {
                        name = getName(rs2.getLong("user"));
                        String STMT3 = "SELECT COUNT(postLike) AS postLikes FROM PostLike WHERE PostLike.topicId = ?";
                        try(PreparedStatement p3 = c.prepareStatement(STMT3)) {
                            p3.setLong(1, topicId);
                            ResultSet rs3 = p3.executeQuery();
                            while(rs3.next()){
                                PostView PV = new PostView(rs.getLong("forumId"), topicId,
                                rs2.getInt("postId"), name, rs2.getString("username"),
                                rs2.getString("text"), rs2.getString("createdAt"), rs3.getInt("postLikes"));
                                return Result.success(PV);
                            }
                            return Result.success();
                        } catch (SQLException e) {
                            return Result.fatal(e.getMessage());
                        }
                    } else return Result.failure("No post with this number exists.");
                } catch(SQLException e) {
                    return Result.fatal(e.getMessage());
                }
            } else return Result.failure("No topic with this id exists.");
        } catch (SQLException e) {
            return Result.fatal(e.getMessage());
        }
    }

    @Override
    public Result createPost(long topicId, String username, String text) {
        Long value = getUserId(username);
        Integer postId = countPostsInTopic(topicId).getValue() + 1;
        String STMT = "INSERT Post (postId, text, topicId, user, createdAt) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            p.setInt(1, postId);
            p.setString(2, text);
            p.setLong(3, topicId);
            p.setLong(4, value);
            p.setTimestamp(5, new Timestamp(System.currentTimeMillis()));
            int checkCreate = p.executeUpdate();
            if (checkCreate < 1) {
                return Result.failure("createPost: Insertion failed");
            }
            else {
                c.commit();
                return Result.success();
            }
        } catch (SQLException e) {
            if (e instanceof SQLIntegrityConstraintViolationException)
            return Result.failure("createPost: Post exists");
            try {
                c.rollback();
            } catch (SQLException f) {
                return Result.fatal(f.getMessage());
            }
            return Result.fatal(e.getMessage());
        }
    }

    @Override
    public Result createTopic(long forumId, String username, String title, String text) {
        Long topicId = null;
        String STMT = "INSERT Topic (forumId, owner, title) VALUES (?, ?, ?)";
        try (PreparedStatement p = c.prepareStatement(STMT, Statement.RETURN_GENERATED_KEYS)) {
            p.setLong(1, forumId);
            p.setLong(2, getUserId(username));
            p.setString(3, title);
            //p.setString(4, text);
            int checkCreate = p.executeUpdate();
            ResultSet rs = p.getGeneratedKeys();
            if (rs.next()) {
                topicId = rs.getLong(1);
            }
            if (checkCreate < 1) {
                return Result.failure("createTopic: Insertion failed");
            }
            createPost(topicId, username, text);
            c.commit();
            return Result.success();
        } catch (SQLException e) {
            if (e instanceof SQLIntegrityConstraintViolationException)
                return Result.failure("createTopic: Topic exists");
            try {
                c.rollback();
            } catch (SQLException f) {
                return Result.fatal(f.getMessage());
            }
            return Result.fatal(e.getMessage());
        }
    }

    private Long getUserId(String username) {
        Long value = null;
        String STMT = "SELECT id FROM Person WHERE username = ?";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            p.setString(1, username);
            ResultSet rs = p.executeQuery();
            if(rs.next()) {
                value = rs.getLong("id");
            }
            return value;
        } catch (SQLException e) {
            throw new RuntimeException(e+"Can't find User Id");
        }
    }

    @Override
    public Result<Integer> countPostsInTopic(long topicId) {
        try (PreparedStatement p = c.prepareStatement(
            "SELECT COUNT(postId) AS count FROM Post WHERE topicId = ?")) {
            p.setLong(1, topicId);
            ResultSet rs = p.executeQuery();
            if (rs.next()) {
                return Result.success(rs.getInt("count"));
            } else {
                return Result.failure("No topic exists with this ID, therefore zero posts.");
            }
        } catch (SQLException e) {
            return Result.fatal(e.getMessage());
        }
    }

    /* B.1 */

    @Override
    public Result likeTopic(String username, long topicId, boolean like) {
        Long userId = getUserId(username);
        String STMT = "INSERT INTO TopicLike(user, topicId, topicLike) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE topicLike=true";
        String STMT2 = "INSERT INTO TopicLike(user, topicId, topicLike) VALUES (?, ?, ?) ON DUPLICATE KEY UPDATE topicLike=false";
        if (like) {
            try (PreparedStatement p = c.prepareStatement(STMT)) {
                p.setLong(1, userId);
                p.setLong(2, topicId);
                p.setBoolean(3, true);
                p.executeQuery();
                c.commit();
            }
            catch (SQLException e) {
                try {
                    c.rollback();
                } catch (SQLException f) {
                    return Result.fatal("Unable to rollback changes.");
                }
                return Result.fatal(e.getMessage());
            }
        }
        else{
            try (PreparedStatement p = c.prepareStatement(STMT2)) {
                p.setLong(1, userId);
                p.setLong(2, topicId);
                p.setBoolean(3, false);
                p.executeQuery();
                c.commit();
            }
            catch (SQLException e) {
                try {
                    c.rollback();
                } catch (SQLException f) {
                    return Result.fatal("Unable to rollback changes.");
                }
                return Result.fatal(e.getMessage());
            }
        }
        return Result.success();
    }

    @Override
    public Result likePost(String username, long topicId, int post, boolean like) {
        Long userId = getUserId(username);
        String STMT = "INSERT INTO PostLike(user, topicId, post, postLike) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE postLike=true";
        String STMT2 = "INSERT INTO PostLike(user, topicId, post, postLike) VALUES (?, ?, ?, ?) ON DUPLICATE KEY UPDATE postLike=false";
        if (like) {
            try (PreparedStatement p = c.prepareStatement(STMT)) {
                p.setLong(1, userId);
                p.setLong(2, topicId);
                p.setInt(3, post);
                p.setBoolean(4, true);
                p.executeQuery();
                c.commit();
            }
            catch (SQLException e) {
                try {
                    c.rollback();
                } catch (SQLException f) {
                    return Result.failure(f.getMessage());
                }
                return Result.fatal(e.getMessage());
            }
        }
        else {
            try (PreparedStatement p = c.prepareStatement(STMT2)) {
                p.setLong(1, userId);
                p.setLong(2, topicId);
                p.setInt(3, post);
                p.setBoolean(4, false);
                p.executeQuery();
                c.commit();
            }
            catch (SQLException e) {
                try {
                    c.rollback();
                } catch (SQLException f) {
                    return Result.fatal(f.getMessage());
                }
                return Result.fatal(e.getMessage());
            }
        }
        return Result.success();
    }

    // getLikers should be modified
    @Override
    public Result<List<PersonView>> getLikers(long topicId) {
        List<PersonView> PVlist = new ArrayList<>();
        try(PreparedStatement p = c.prepareStatement("SELECT username, name, stuId FROM Person LEFT JOIN TopicLike ON Person.id=TopicLike.user WHERE TopicLike.topicId=? AND TopicLike.topiclike=1 ORDER BY name")){
            p.setLong(1, topicId);
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                PersonView pview = new PersonView(rs.getString("name"), rs.getString("username"), rs.getString("stuId"));
                PVlist.add(pview);
            }
            return Result.success(PVlist);
        }catch (SQLException e){
            return Result.fatal(e.getMessage());
        }
    }

    @Override
    public Result<TopicView> getTopic(long topicId) {
        List<PostView> PVlist = new ArrayList<>();
    //  PostView PV = null;
        String STMT = "SELECT forumId, Topic.id, Forum.title AS forumTitle, Topic.Title AS topicTitle FROM Topic JOIN Forum ON Topic.forumId = Forum.id WHERE Topic.id = ?";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            p.setLong(1, topicId);
            ResultSet rs = p.executeQuery();
            if(rs.next()) {
                String STMT2 = "SELECT Topic.forumId, Post.topicId, Post.postId, Post.user, Person.username, Post.text, Post.createdAt, postlike FROM Post JOIN Person ON Post.user = Person.id LEFT JOIN PostLike ON post = Post.postId JOIN Topic ON Topic.id = Post.topicId WHERE Post.topicId = ? GROUP BY postId, Topic.forumId, Post.topicId, Post.user, Person.username, Post.text, Post.createdAt, postlike";
                try(PreparedStatement p1 = c.prepareStatement(STMT2)) {
                    p1.setLong(1, topicId);
                    ResultSet rs2 = p1.executeQuery();
                    while(rs2.next()) {
                        PostView PV = new PostView(rs2.getLong("forumId"), rs2.getLong("topicId"), rs2.getInt("postId"), rs2.getString("user"),
                        rs2.getString("username"), rs2.getString("text"), rs2.getString("createdAt"), rs2.getInt("postlike"));
                        PVlist.add(PV);
                    }
                    TopicView TV = new TopicView(rs.getLong("forumId"), rs.getLong("id"), rs.getString("forumTitle"), rs.getString("topicTitle"), PVlist);
                    return Result.success(TV);
                } catch (SQLException e) {
                    return Result.fatal(e.getMessage());
                }
            }
        } catch (SQLException e){
            return Result.fatal(e.getMessage());
        }
        return Result.success();
    }

    /* B.2 */

    @Override
    public Result<List<AdvancedForumSummaryView>> getAdvancedForums() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Result<AdvancedPersonView> getAdvancedPersonView(String username) {
        Long userId = getUserId(username);
        List<TopicSummaryView> TSV = new ArrayList<>();
        String name = null, stuId = null;
        Integer topicLikes = 0, postLikes = 0;
        String STMT = "SELECT name, stuId, topicLikes, postLikes FROM (SELECT Person.name AS name, Person.stuId AS stuId, COUNT(TopicLike.topicLike) AS topicLikes FROM TopicLike JOIN Topic ON Topic.id = TopicLike.topicId JOIN Person ON Topic.owner = Person.id WHERE Person.id = ? and TopicLike.topicLike = true GROUP BY Person.name, Person.stuId) AS c0 JOIN (SELECT COUNT(PostLike.postLike) AS postLikes FROM PostLike JOIN Post ON PostLike.post = Post.postId AND PostLike.topicId = Post.topicId JOIN Person ON Post.user = Person.id WHERE Person.id = ? and PostLike.postLike = true) AS c1";
        String STMT1 = "SELECT Topic.id, Topic.forumId, Topic.title, Topic.created, Topic.owner AS creatorId FROM Topic LEFT JOIN TopicLike ON TopicLike.topicId = Topic.id WHERE TopicLike.topicId IN (SELECT TopicLike.topicId FROM TopicLike RIGHT JOIN Person ON Person.id = TopicLike.user WHERE Person.id = ? and TopicLike.topicLike = true) GROUP BY Topic.id, Topic.forumId, Topic.title, Topic.created, creatorId";
        try(PreparedStatement p = c.prepareStatement(STMT)) {
          p.setLong(1, userId);
          p.setLong(2, userId);
          ResultSet rs = p.executeQuery();
          if(rs.next()) {
              name = rs.getString("name");
              stuId = rs.getString("stuId");
              topicLikes = rs.getInt("topicLikes");
              postLikes = rs.getInt("postLikes");
          }
        } catch (SQLException e) {
          return Result.fatal(e.getMessage());
        }

        try(PreparedStatement p1 = c.prepareStatement(STMT1)) {
          p1.setLong(1, userId);
          ResultSet rs1 = p1.executeQuery();
          while(rs1.next()) {
              TopicSummaryView TSV1 = new TopicSummaryView(rs1.getLong("id"),
                  rs1.getLong("forumId"), rs1.getString("title"), countPostsInTopic(rs1.getLong("id")).getValue(),
                  rs1.getString("created"), getLatestPost(rs1.getLong("id")).getValue().getPostedAt(),
                  getLatestPost(rs1.getLong("id")).getValue().getAuthorName(),
                  getLikes(rs1.getLong("id")), getName(rs1.getLong("creatorId")), getUserName(rs1.getLong("creatorId")));
              TSV.add(TSV1);
          }
        } catch (SQLException e){
          return Result.fatal(e.getMessage());
        }
        name = getName(userId);
        stuId = getStuId(userId);
        AdvancedPersonView APV = new AdvancedPersonView(name, username, stuId, topicLikes,
          postLikes, TSV);
        return Result.success(APV);
        }

    private Integer getLikes(long topicId) {
        int likes = 0;
        try (PreparedStatement p = c.prepareStatement(
            "SELECT COUNT(topicLike) AS count FROM TopicLike WHERE topicId = ?")) {
            p.setLong(1, topicId);
            ResultSet rs = p.executeQuery();
            if (rs.next()) {
                likes = rs.getInt("count");
            }
            return likes;
        } catch (SQLException e) {
            throw new RuntimeException(e+"Can't find likes");
        }
    }

    private String getName(long userId) {
        String name = null;
        String STMT = "SELECT name FROM Person WHERE id = ?";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            p.setLong(1, userId);
            ResultSet rs = p.executeQuery();
            if(rs.next()){
                name = rs.getString("name");
            }
            return name;
        } catch (SQLException e) {
            throw new RuntimeException(e+"Can't find name");
        }
    }

    private String getUserName(long userId) {
        String username = null;
        String STMT = "SELECT username FROM Person WHERE id = ?";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            p.setLong(1, userId);
            ResultSet rs = p.executeQuery();
            if(rs.next()){
                username = rs.getString("username");
            }
            return username;
        } catch (SQLException e) {
            throw new RuntimeException(e+"Can't find username");
        }
    }

    private String getStuId(long userId) {
        String username = null;
        String STMT = "SELECT stuId FROM Person WHERE id = ?";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            p.setLong(1, userId);
            ResultSet rs = p.executeQuery();
            if(rs.next()){
                username = rs.getString("stuId");
            }
            return username;
        } catch (SQLException e) {
            throw new RuntimeException(e+"Can't find stuId");
        }
    }

    @Override
    public Result<AdvancedForumView> getAdvancedForum(long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
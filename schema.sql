DROP TABLE IF EXISTS PostLike;
DROP TABLE IF EXISTS TopicLike;
DROP TABLE IF EXISTS Post;
DROP TABLE IF EXISTS Topic;
DROP TABLE IF EXISTS Forum;
DROP TABLE IF EXISTS Person;

CREATE TABLE Person (
 id INTEGER PRIMARY KEY AUTO_INCREMENT,
 name VARCHAR(100)  NOT NULL,
 username VARCHAR(100)  NOT NULL UNIQUE,
 stuId VARCHAR(100)  NULL
);

CREATE TABLE Forum (
id   INTEGER   PRIMARY KEY AUTO_INCREMENT,
title   VARCHAR(100) NOT NULL UNIQUE
);

CREATE TABLE Topic (
id   INTEGER   PRIMARY KEY AUTO_INCREMENT,
title   VARCHAR(100)  NOT NULL,
forumId INTEGER   NOT NULL,
owner   INTEGER    NOT NULL,
/*text   TEXT     NOT NULL,*/
created TIMESTAMP NOT NULL,
FOREIGN KEY (forumId) REFERENCES Forum(id),
FOREIGN KEY (owner) REFERENCES Person(id)
);

CREATE TABLE Post (
postId INTEGER   NOT NULL,
text    TEXT    NOT NULL,
topicId INTEGER    NOT NULL,
user    INTEGER   NOT NULL,
createdAt TIMESTAMP   NOT NULL,
FOREIGN KEY (topicId) REFERENCES Topic(id),
FOREIGN KEY (user) REFERENCES Person(id),
PRIMARY KEY (postId, topicId)
);

CREATE TABLE TopicLike (
user   INTEGER    NOT NULL ,
topicId INTEGER   NOT NULL,
topiclike   BOOLEAN   NULL,
FOREIGN KEY (user) REFERENCES Person(id),
FOREIGN KEY (topicId) REFERENCES Topic(id),
PRIMARY KEY (user, topicId)
);

CREATE TABLE PostLike (
user   INTEGER    NOT NULL,
topicId INTEGER   NOT NULL,
post   INTEGER   NOT NULL,
postlike  BOOLEAN   NULL,
FOREIGN KEY (user) REFERENCES Person(id),
FOREIGN KEY (topicId) REFERENCES Topic(id),
FOREIGN KEY (post) REFERENCES Post(postId),
PRIMARY KEY (user, topicId, post)
);

/*INSERT INTO Person (id, name, username, stuId) VALUES (1, 'Nadia', 'nad', 11);
INSERT INTO Person (id, name, username, stuId) VALUES (2, 'Priscila', 'pris', 22);
INSERT INTO Person (id, name, username, stuId) VALUES (3, 'Connie', 'conn', 33);
INSERT INTO Person (id, name, username, stuId) VALUES (4, 'Gwen', 'gwen', 44);
INSERT INTO Person (id, name, username, stuId) VALUES (5, 'Lance', 'lan', 55);

INSERT INTO Forum (id, title) VALUES (1, 'Databases');
INSERT INTO Forum (id, title) VALUES (2, 'Object Oriented Programming with Java');
INSERT INTO Forum (id, title) VALUES (3, 'Web Technologies');

INSERT INTO Topic (id, title, forumId, owner, text, created) VALUES (1, 'SQL', 1, 1, 'First text for SQL','2018-04-11 11:00:00');
INSERT INTO Topic (id, title, forumId, owner, text, created) VALUES (2, 'JDBC', 1, 2, 'First text for JDBC', '2018-04-11 12:30:08');
INSERT INTO Topic (id, title, forumId, owner, text, created) VALUES (3, 'OO Design', 2, 2, 'First text for Java OO Design', '2018-04-11 15:47:17');
INSERT INTO Topic (id, title, forumId, owner, text, created) VALUES (4, 'Graphics', 2, 3, 'First text for Java Graphics','2018-04-11 21:59:03');
INSERT INTO Topic (id, title, forumId, owner, text, created) VALUES (5, 'HTML', 3, 4, 'First text for HTML', '2018-04-11 03:03:03');

INSERT INTO Post (postId, text, topicId, user, createdAt) VALUES (1, '1. Post in SQL', 1, 1, '2018-04-11 11:00:00');
INSERT INTO Post (postId, text, topicId, user, createdAt) VALUES (2, '2. Post in SQL', 1, 2, '2018-04-11 12:30:08');
INSERT INTO Post (postId, text, topicId, user, createdAt) VALUES (1, '1. Post in JDBC', 2, 3, '2018-04-11 15:47:17');
INSERT INTO Post (postId, text, topicId, user, createdAt) VALUES (2, '2. Post in JDBC', 2, 4, '2018-04-11 21:59:03');
INSERT INTO Post (postId, text, topicId, user, createdAt) VALUES (1, '1. Post in OO Design', 3, 5, '2018-04-11 03:03:03');
INSERT INTO Post (postId, text, topicId, user, createdAt) VALUES (2, '2. Post in OO Design', 3, 1, '2018-04-11 11:00:00');
INSERT INTO Post (postId, text, topicId, user, createdAt) VALUES (1, '1. Post in Graphics', 4, 2, '2018-04-11 11:00:00');
INSERT INTO Post (postId, text, topicId, user, createdAt) VALUES (2, '2. Post in Graphics', 4, 3, '2018-04-11 11:00:00');
INSERT INTO Post (postId, text, topicId, user, createdAt) VALUES (1, '1. Post in HTML', 5, 4, '2018-04-11 11:00:00');
INSERT INTO Post (postId, text, topicId, user, createdAt) VALUES (2, '2. Post in HTML', 5, 5, '2018-04-11 11:00:00');*/


/*SELECT Topic.id, Topic.forumId, Topic.title, SUM(Post.postId), Topic.created, Person.name,


-- This gets the lastPostName
SELECT Person.name FROM Person JOIN Post ON Post.user = Person.id JOIN Topic ON Topic.owner = Person.id ORDER BY createdAt DESC LIMIT BY 1 WHERE Topic = ?;


--
SELECT Topic.id, Topic.forumId, Topic.title, SUM(Post.postId), Topic.created, Post.createdAt, Person.name, Topic.owner, Person.username, SUM(TopicLike.topicId)FROM Person JOIN Post ON Post.user = Person.id JOIN Topic ON Topic.owner = Person.id JOIN TopicLike ON TopicLike.topicId = Topic.id WHERE Topic.id = 1 ORDER BY createdAt DESC LIMIT 1;*/
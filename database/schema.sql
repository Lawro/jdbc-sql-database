DROP TABLE IF EXISTS PostLike;
DROP TABLE IF EXISTS TopicLike;
DROP TABLE IF EXISTS Post;
DROP TABLE IF EXISTS Topic;
DROP TABLE IF EXISTS Forum;
DROP TABLE IF EXISTS Person;

CREATE TABLE Person (
 	id 		INTEGER 		PRIMARY KEY AUTO_INCREMENT,
 	name 		VARCHAR(100) 	NOT NULL,
 	username 	VARCHAR(100) 	NOT NULL UNIQUE,
 	stuId 		VARCHAR(100) 	NULL
);

CREATE TABLE Forum (
 	id 		INTEGER			PRIMARY KEY AUTO_INCREMENT,
 	title 		VARCHAR(100)	NOT NULL UNIQUE
);

CREATE TABLE Topic (
 	id 		INTEGER			PRIMARY KEY AUTO_INCREMENT,
 	title 		VARCHAR(100)	NOT NULL,
 	forumId	INTEGER			NOT NULL,
 	owner		INTEGER			NOT NULL,
 	text 		TEXT 			NOT NULL,
 	FOREIGN KEY (forumId) REFERENCES Forum(id),
 	FOREIGN KEY (owner) REFERENCES Person(id)
);

CREATE TABLE Post (
 	id 		INTEGER			PRIMARY KEY AUTO_INCREMENT,
 	text 		TEXT			NOT NULL,
 	topicId	INTEGER			NOT NULL,
 	user 		INTEGER			NOT NULL,
 	FOREIGN KEY (topicId) REFERENCES Topic(id),
 	FOREIGN KEY (user) REFERENCES Person(id)
);

INSERT INTO Person (id, name, username, stuId) VALUES (1, 'Nadia', 'nad', 11);
INSERT INTO Person (id, name, username, stuId) VALUES (2, 'Priscila', 'pris', 22);
INSERT INTO Person (id, name, username, stuId) VALUES (3, 'Connie', 'conn', 33);
INSERT INTO Person (id, name, username, stuId) VALUES (4, 'Gwen', 'gwen', 44);
INSERT INTO Person (id, name, username, stuId) VALUES (5, 'Lance', 'lan', 55);

INSERT INTO Forum (id, title) VALUES (1, 'Databases');
INSERT INTO Forum (id, title) VALUES (2, 'Object Oriented Programming with Java');
INSERT INTO Forum (id, title) VALUES (3, 'Web Technologies');

INSERT INTO Topic (id, title, forumId, owner, text) VALUES (1, 'SQL', 1, 1, 'First text for SQL');
INSERT INTO Topic (id, title, forumId, owner, text) VALUES (2, 'JDBC', 1, 2, 'First text for JDBC');
INSERT INTO Topic (id, title, forumId, owner, text) VALUES (3, 'OO Design', 2, 2, 'First text for Java OO Design');
INSERT INTO Topic (id, title, forumId, owner, text) VALUES (4, 'Graphics', 2, 3, 'First text for Java Graphics');
INSERT INTO Topic (id, title, forumId, owner, text) VALUES (5, 'HTML', 3, 4, 'First text for HTML');

INSERT INTO Post (id, text, topicId, user) VALUES (1, '1. Post in SQL', 1, 1);
INSERT INTO Post (id, text, topicId, user) VALUES (2, '2. Post in SQL', 1, 2);
INSERT INTO Post (id, text, topicId, user) VALUES (3, '1. Post in JDBC', 2, 3);
INSERT INTO Post (id, text, topicId, user) VALUES (4, '2. Post in JDBC', 2, 4);
INSERT INTO Post (id, text, topicId, user) VALUES (5, '1. Post in OO Design', 3, 5);
INSERT INTO Post (id, text, topicId, user) VALUES (6, '2. Post in OO Design', 3, 1);
INSERT INTO Post (id, text, topicId, user) VALUES (7, '1. Post in Graphics', 4, 2);
INSERT INTO Post (id, text, topicId, user) VALUES (8, '2. Post in Graphics', 4, 3);
INSERT INTO Post (id, text, topicId, user) VALUES (9, '1. Post in HTML', 5, 4);
INSERT INTO Post (id, text, topicId, user) VALUES (10, '2. Post in HTML', 5, 5);

INSERT INTO TopicLike (user, topicId, topiclike) VALUES (1, 1, 1);
INSERT INTO TopicLike (user, topicId, topiclike) VALUES (1, 2, 1);
INSERT INTO TopicLike (user, topicId, topiclike) VALUES (2, 1, 1);
INSERT INTO TopicLike (user, topicId, topiclike) VALUES (2, 2, 1);
INSERT INTO TopicLike (user, topicId, topiclike) VALUES (2, 4, 1);

INSERT INTO PostLike (user, postId, postlike) VALUES (1, 1, 1);
INSERT INTO PostLike (user, postId, postlike) VALUES (1, 2, 1);
INSERT INTO PostLike (user, postId, postlike) VALUES (2, 1, 1);
INSERT INTO PostLike (user, postId, postlike) VALUES (2, 2, 1);
INSERT INTO PostLike (user, postId, postlike) VALUES (2, 3, 1);
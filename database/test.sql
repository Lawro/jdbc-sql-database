DROP TABLE IF EXISTS PostLike;

DROP TABLE IF EXISTS TopicLike;

DROP TABLE IF EXISTS Post;

DROP TABLE IF EXISTS Topic;

DROP TABLE IF EXISTS Forum;

DROP TABLE IF EXISTS Person;



CREATE TABLE Person (

 id         INTEGER         PRIMARY KEY AUTO_INCREMENT,

 name       VARCHAR(100)    NOT NULL,

 username   VARCHAR(10)     NOT NULL UNIQUE,

 stuId      VARCHAR(10)     NULL

);



CREATE TABLE Forum (

 id         INTEGER         PRIMARY KEY AUTO_INCREMENT,

 title      VARCHAR(100)    NOT NULL UNIQUE

);



CREATE TABLE Topic (

 id         INTEGER         PRIMARY KEY AUTO_INCREMENT,

 title      VARCHAR(100)    NOT NULL,

 forumId    INTEGER         NOT NULL,

 owner      VARCHAR(10)     NOT NULL,

 text       TEXT            NOT NULL,

 FOREIGN KEY (forumId) REFERENCES Forum(id),

 FOREIGN KEY (owner) REFERENCES Person(username)

);



CREATE TABLE Post (

 id         INTEGER         PRIMARY KEY AUTO_INCREMENT,

 text       TEXT            NOT NULL,

 topicId    INTEGER         NOT NULL,

 user       INTEGER         NOT NULL,

 FOREIGN KEY (topicId) REFERENCES Topic(id),

 FOREIGN KEY (user) REFERENCES Person(id)

);



CREATE TABLE TopicLike (

 username   VARCHAR(10)     NOT NULL,

 topicId    INTEGER         NOT NULL,

 topiclike       BOOLEAN         NULL,

 FOREIGN KEY (username) REFERENCES Person(username),

 FOREIGN KEY (topicId) REFERENCES Topic(id),

 PRIMARY KEY (username, topicId)

);



CREATE TABLE PostLike (

 username   VARCHAR(10)     NOT NULL,

 topicId    INTEGER         NOT NULL,

 postId     INTEGER         NOT NULL,

 postlike       BOOLEAN         NULL,

 FOREIGN KEY (username) REFERENCES Person(username),

 FOREIGN KEY (topicId) REFERENCES Topic(id),

 FOREIGN KEY (postId) REFERENCES Post(id),

 PRIMARY KEY (username, postId)

);
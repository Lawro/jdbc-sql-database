package uk.ac.bris.cs.databases.cwk3;
// imports

import java.sql.Connection;
/*import java.util.List;
import java.util.Map;*/
import java.util.*;
import uk.ac.bris.cs.databases.api.APIProvider;
import uk.ac.bris.cs.databases.api.AdvancedForumSummaryView;
import uk.ac.bris.cs.databases.api.AdvancedForumView;
import uk.ac.bris.cs.databases.api.ForumSummaryView;
import uk.ac.bris.cs.databases.api.ForumView;
import uk.ac.bris.cs.databases.api.AdvancedPersonView;
import uk.ac.bris.cs.databases.api.PostView;
import uk.ac.bris.cs.databases.api.Result;
import uk.ac.bris.cs.databases.api.PersonView;
import uk.ac.bris.cs.databases.api.SimpleForumSummaryView;
import uk.ac.bris.cs.databases.api.SimpleTopicView;
import uk.ac.bris.cs.databases.api.TopicView;

// Additional databases.api imports
import uk.ac.bris.cs.databases.api.SimpleTopicSummaryView;
import uk.ac.bris.cs.databases.api.SimplePostView;

// SQL imports
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.DriverManager;
import java.sql.SQLIntegrityConstraintViolationException;
import java.sql.Timestamp;
import java.sql.*;

/**
 *
 * @author csxdb
 */
public class API implements APIProvider {

    private final Connection c;
    
    public API(Connection c) {
        this.c = c;
    }

    /* A.1 */
    @Override
    public Result<Map<String, String>> getUsers() {
        Map<String, String> map = new HashMap<String, String>();
        String STMT = "SELECT username, name FROM Person";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                map.put(rs.getString("username"), rs.getString("name"));
            }
            return Result.success(map);
        } catch (SQLException e) {
            return Result.fatal(e.getMessage());
        }
    }
    

    @Override
    public Result<PersonView> getPersonView(String username) {
        if (username == null || username.isEmpty()) {
            return Result.failure("getPersonView: username cannot be empty");
        }
        String STMT = "SELECT username, name, stuId FROM Person WHERE username = ?";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            p.setString(1, username);
            ResultSet rs = p.executeQuery();
            if (rs.next()){
                PersonView pview = new PersonView(rs.getString("name"),
                    rs.getString("username"), rs.getString("stuId"));
                return Result.success(pview);
            }
            else {
                return Result.failure("getPersonView: User doesn't exist");
            }
        } catch (SQLException e) {
            return Result.fatal(e.getMessage());
        }
    }


    @Override
    public Result addNewPerson(String name, String username, String studentId) {
        if (name == null || name.isEmpty()) {
            return Result.failure("addNewPerson: name cannot be empty");
        }
        if (username == null || username.isEmpty()) {
            return Result.failure("addNewPerson: username cannot be empty");
        }
        String STMT = "INSERT INTO Person (name, username, stuId)"+
                "VALUES (?,?,?)";
        try (PreparedStatement p = c.prepareStatement(STMT)){
            studentId = studentId==null ? "" : studentId;
            p.setString(1, name);
            p.setString(2, username);
            p.setString(3, studentId);
            p.executeUpdate();
            c.commit();
            return Result.success();
        } catch (SQLException e) {
            if (e instanceof SQLIntegrityConstraintViolationException)
                return Result.failure("addNewPerson: Username exists");
            else
                return Result.fatal(e.getMessage());
        }
    }

    
    /* A.2 */

    @Override
    public Result<List<SimpleForumSummaryView>> getSimpleForums() {
        List<SimpleForumSummaryView> ForumList = new ArrayList<>();
        final String STMT = "SELECT * FROM Forum";
        try (PreparedStatement p = c.prepareStatement(STMT)){
            ResultSet rs = p.executeQuery();
            while (rs.next()) {
                SimpleForumSummaryView forum = new SimpleForumSummaryView (
                    rs.getLong("id"), rs.getString("title"));
                ForumList.add(forum);
            }
            return Result.success(ForumList);
        } catch (SQLException e) {
            return Result.fatal(e.getMessage());
        }
    }


    @Override
    public Result createForum(String title) {
        if (title == null || title.isEmpty()) {
            return Result.failure("Cannot have null or empty title.");
        }
        String STMT = "INSERT INTO Forum (title) VALUES (?)";
        try (PreparedStatement p = c.prepareStatement(STMT)) {
            p.setString(1, title);
            p.executeUpdate();
            c.commit();
            return Result.success();
        } catch (SQLException e) {
            if (e instanceof SQLIntegrityConstraintViolationException)
                return Result.failure("createForum: Title exists");
            else
                return Result.fatal(e.getMessage());
        }
    }

    /* A.3 */
 
    @Override
    public Result<List<ForumSummaryView>> getForums() {
        List<ForumSummaryView> forumList = new ArrayList<>();
        final String forumSTMT = "SELECT * FROM Forum";
        final String topicSTMT = "SELECT * FROM Topic WHERE forumId = ?";
        try (PreparedStatement p0 = c.prepareStatement(forumSTMT)) {
            ResultSet rs0 = p0.executeQuery();
            while (rs0.next()) {
                try (PreparedStatement p1 = c.prepareStatement(topicSTMT)) {
                    p1.setString(1, rs0.getString("id"));
                    SimpleTopicSummaryView STSV = getTopic(p1);
                    ForumSummaryView FSV = new ForumSummaryView(rs0.getLong("id"),
                        rs0.getString("title"), STSV);
                    forumList.add(FSV);
                }
            }
            return Result.success(forumList);
        } catch (SQLException e) {
            return Result.fatal(e.getMessage());
        }
    }



    private SimpleTopicSummaryView getTopic(PreparedStatement p1) {
        SimpleTopicSummaryView STSV = null;
        try {
            ResultSet rs1 = p1.executeQuery();
            if (rs1.next()) {
                STSV = new SimpleTopicSummaryView(rs1.getLong("id"),
                    rs1.getLong("forumId"), rs1.getString("title"));
            }
            return STSV;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    @Override
        public Result<ForumView> getForum(long id) {
        List<SimpleTopicSummaryView> STSVlist = new LinkedList<>();
        String STMT1 = "SELECT * FROM Forum WHERE id = ?";
        try (PreparedStatement p = c.prepareStatement(STMT1)) {
            p.setLong(1, id);
            ResultSet rs = p.executeQuery();
            if (rs.next()) {
                String STMT2 = "SELECT id, forumId, title FROM Topic WHERE forumId = ?";
                try (PreparedStatement p2 = c.prepareStatement(STMT2)) {
                    p2.setLong(1, id);
                    ResultSet rs2 = p2.executeQuery();
                    while (rs2.next()) {
                    //SimpleTopicSummaryView STSV = getTopic(p2);
                        SimpleTopicSummaryView STSV = new SimpleTopicSummaryView(
                        rs2.getLong("id"), rs2.getLong("forumid"), rs2.getString("title"));
                        STSVlist.add(STSV);
                    }
                    ForumView FV = new ForumView(id, rs.getString("title"), STSVlist);
                    return Result.success(FV);
                } catch (SQLException e) {
                return Result.fatal(e.getMessage());
                }
            }
            else return Result.failure("No forum with this id exists.");
        } catch (SQLException e) {
        return Result.fatal(e.getMessage());
        }
    }

   private List<SimpleTopicSummaryView> getTopics(PreparedStatement p1) {
       List<SimpleTopicSummaryView> STSV1 = new LinkedList<>();
       SimpleTopicSummaryView STSV = null;
       try {
           ResultSet rs1 = p1.executeQuery();
           while (rs1.next()) {
               STSV = new SimpleTopicSummaryView(rs1.getLong("topicId"),
                   rs1.getLong("forumId"), rs1.getString("title"));
               STSV1.add(STSV);
           }
           return STSV1;
       } catch (SQLException e) {
           throw new RuntimeException(e);
       }
   }

    @Override
    public Result<SimpleTopicView> getSimpleTopic(long topicId) {
        List<SimplePostView> STSVlist = new LinkedList<>();
        String STMT1 = "SELECT title FROM Topic WHERE id = ?";
        try (PreparedStatement p = c.prepareStatement(STMT1)) {
            p.setLong(1, topicId);
            ResultSet rs = p.executeQuery();
            if (rs.next()) {
                String STMT2 = 
                    "SELECT postId, name, text, createdAt FROM Post INNER JOIN Person ON (Person.id = user) WHERE topicId = ?";
                try (PreparedStatement p2 = c.prepareStatement(STMT2)) {
                    p2.setLong(1, topicId);
                    ResultSet rs2 = p2.executeQuery();
                    while (rs2.next()) {
                        SimplePostView SPV = new SimplePostView(rs2.getInt("postId"), 
                            rs2.getString("name"), rs2.getString("text"), rs2.getString("createdAt"));
                        STSVlist.add(SPV);
                    }
                    SimpleTopicView STV = new SimpleTopicView(topicId, 
                        rs.getString("title"), STSVlist);
                    return Result.success(STV);
                } catch (SQLException e) {
                    return Result.fatal(e.getMessage());
                }
            }
            else return Result.failure("No topic with this id exists.");
        } catch (SQLException e) {
            return Result.fatal(e.getMessage());
        }
    }


    @Override
    public Result<PostView> getLatestPost(long topicId) {
        String STMT1 = "SELECT forumId FROM Topic WHERE id = ?";
        try(PreparedStatement p = c.prepareStatement(STMT1)){
            p.setLong(1, topicId);
            ResultSet rs = p.executeQuery();
            if(rs.next()){
                String STMT2 = "SELECT topicId, postId, user, username, text, createdAt FROM Post JOIN Person ON Post.user = Person.id WHERE topicId = ? ORDER BY Post.createdAt DESC LIMIT 1";
                try(PreparedStatement p2 = c.prepareStatement(STMT2)){
                    p2.setLong(1, topicId);
                    ResultSet rs2 = p2.executeQuery();
                    if(rs2.next()){
                        String STMT3 = "SELECT SUM(postLike), topicId FROM PostLike JOIN Post ON PostLike.likeId = Post.postId WHERE topicId = ? GROUP BY postId";
                        try(PreparedStatement p3 = c.prepareStatement(STMT3)){
                            p3.setLong(1, topicId);
                            ResultSet rs3 = p3.executeQuery();
                            while(rs3.next()){
                                PostView PV = new PostView(rs.getLong("forumId"), topicId,
                                rs2.getInt("postNumber"), rs2.getString("authorName"), rs2.getString("authorUserName"),
                                rs2.getString("text"), rs2.getString("postedAt"), rs3.getInt("likes"));
                                return Result.success(PV);
                        }
                        return Result.success();
                        } catch (SQLException e){
                            return Result.fatal(e.getMessage());
                        }
                    } else return Result.failure("No post with this number exists.");
                } catch(SQLException e){
                    return Result.fatal(e.getMessage());
                }
            }   else return Result.failure("No topic with this id exists.");
        } catch (SQLException e){
            return Result.fatal(e.getMessage());
        }
    }



    @Override
    public Result createPost(long topicId, String username, String text) {
      Long value = null;
      String STMT1 = "SELECT Person.id FROM Post JOIN Person ON Post.user = Person.id WHERE Person.username = ?";
      try (PreparedStatement p = c.prepareStatement(STMT1)) {
          p.setString(1, username);
          ResultSet rs = p.executeQuery();
          if(rs.next()){
          value = rs.getLong("id");
          String STMT2 = "INSERT Post (topicId, user, text, createdAt) VALUES (?, ?, ?, ?)";
            try (PreparedStatement p1 = c.prepareStatement(STMT2)){
              p1.setLong(1, topicId);
              p1.setLong(2, value);
              p1.setString(3, text);
              p1.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
              int checkCreate = p1.executeUpdate();
              if (checkCreate < 1) {
                  return Result.failure("createPost: Insertion failed");
              }
            else {
              c.commit();
              return Result.success();
          }
      } catch (SQLException e) {
          if (e instanceof SQLIntegrityConstraintViolationException)
              return Result.failure("createPost: Topic exists");
          else
              return Result.fatal(e.getMessage());
      }
    }
    return Result.success();
    } catch (SQLException e) {
        if (e instanceof SQLIntegrityConstraintViolationException)
            return Result.failure("createPost: Topic exists");
        else
            return Result.fatal(e.getMessage());
    }
  }


    @Override
    public Result createTopic(long forumId, String username, String title, String text) {
        int topicId = 0;
        String STMT2 = "INSERT Topic (forumId, owner, title, text) VALUES (?, ?, ?, ?)";
        try (PreparedStatement p1 = c.prepareStatement(STMT2, Statement.RETURN_GENERATED_KEYS)) {
            p1.setLong(1, forumId);
            p1.setLong(2, getUserId(username));
            p1.setString(3, title);
            p1.setString(4, text);
            int checkCreate = p1.executeUpdate();
            ResultSet rs1 = p1.getGeneratedKeys();
            if (rs1.next()) {
                topicId = rs1.getInt(1);
            }
            if (checkCreate < 1) {
                return Result.failure("createTopic: Insertion failed");
            }
            else {
                createPost(topicId, username, text);
                c.commit();
                return Result.success();
            }
        } catch (SQLException e) {
            if (e instanceof SQLIntegrityConstraintViolationException)
              return Result.failure("createTopic: Topic exists");
            else
              return Result.fatal(e.getMessage());
        }
    }

    private Long getUserId(String username) {
      Long value = null;
      String STMT1 = "SELECT Person.id FROM Topic JOIN Person ON Topic.owner = Person.id WHERE Person.username = ?";
      try (PreparedStatement p = c.prepareStatement(STMT1)) {
          p.setString(1, username);
          ResultSet rs = p.executeQuery();
          if(rs.next()){
             value = rs.getLong(1);
          }
          return value;
      } catch (SQLException e) {
           throw new RuntimeException(e);      
      }
    }
    
    /*@Override
    public Result<Integer> countPostsInTopic(long topicId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }*/

    @Override
    public Result<Integer> countPostsInTopic(long topicId) {
        try (PreparedStatement p = c.prepareStatement(
            "SELECT COUNT(postId) AS count FROM Post WHERE topic = ?")) {
            p.setLong(1, topicId);
            ResultSet rs = p.executeQuery();
            if (rs.next()) {
                return Result.success(rs.getInt("count"));
            } else {
                return Result.failure("No topic exists with this ID, therefore zero posts.");
            }
        } catch (SQLException e) {
             return Result.fatal(e.getMessage());
        }
    }

    /* B.1 */
       
    /*@Override
    public Result likeTopic(String username, long topicId, boolean like) {
        throw new UnsupportedOperationException("Not supported yet.");
    }*/

    @Override
    public Result likeTopic(String username, long topicId, boolean like) {
        if (like) {
            try (PreparedStatement p = c.prepareStatement("INSERT IGNORE INTO TopicLike(user, topicId) VALUES (?, ?)")) {
                p.setLong(2, topicId);
                p.setString(1, username);
                p.executeQuery();
                c.commit();
            }
            catch (SQLException e) {
                try {
                    c.rollback();
                } catch (SQLException e1) {
                    return Result.fatal("Unable to rollback changes.");
                }
                return Result.fatal(e.getMessage());
            }
        }
        else{
            try (PreparedStatement p = c.prepareStatement("DELETE FROM TopicLike WHERE user = ? AND topicId=?")) {
                p.setLong(2, topicId);
                p.setString(1, username);
                p.executeQuery();
                c.commit();
            }
            catch (SQLException e) {
                try {
                    c.rollback();
                } catch (SQLException e1) {
                    return Result.fatal("Unable to rollback changes.");
                }
                return Result.fatal(e.getMessage());
            }
        }
        return Result.success();
    }
    
    @Override
    public Result likePost(String username, long topicId, int post, boolean like) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Result<List<PersonView>> getLikers(long topicId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Result<TopicView> getTopic(long topicId) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    /* B.2 */

    @Override
    public Result<List<AdvancedForumSummaryView>> getAdvancedForums() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Result<AdvancedPersonView> getAdvancedPersonView(String username) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Result<AdvancedForumView> getAdvancedForum(long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
